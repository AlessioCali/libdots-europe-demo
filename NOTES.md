# Regarding ARM compatibility with libdots

## Compiler.h
* You can check ```#ifdef __arm__```.
* ARM C compilers already have int32/64 defined, so it is possible to skip them.

## Gridprobability.c

* FACTOR_B is defined as 3L... etc etc. Long types are 32 bits in ARM. Redefine to 3LL.

## General

* For some ungodly reason ARM chars are unsigned. This screws up most of the computation. It is necessary to redefine any char to signed char. I would advice defining some preprocessor definition like HAS_UNSIGNED_CHAR and then printing 'signed' before whenever 'char' is present according to that. Something like:

```c
#ifdef __arm__
#define HAS_UNSIGNED_CHAR
#endif
```
...
```c
#ifdef HAS_UNSIGNED_CHAR
signed
#endif
char somevalue = 0;
```