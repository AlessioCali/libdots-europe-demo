# Libdots Europe-Scanner App Demo

This is a small project I developed for a Computer Vision course, which involved prototyping and building a raspberry-based reader for the libdots library. You can read more about libdots at Chili's personal page.

[LibDots](https://chili.epfl.ch/libdots)

Think of this more like a practical exercise in getting some fundamentals in 3D printing, system programming and DIY prototyping (the prototype became much bulkier than I would have liked). The reader runs on a Raspberry Pi Zero W for which a three-level casing has been designed using SketchUp. 

A camera is placed on bottom of the casing, with four white leds providing lighting. A microswitch exploits the device's weight to detect when the reader is put on top of a surface, thus activating the LEDs only when needed and signalling the Raspberry to start acquiring readings from the camera.

![Bottom view](http://g4f.altervista.org/wp-content/uploads/2018/04/photo_2018-04-13_21-23-54-e1523654983703.jpg)

The mid layer hosts the Raspberry itself and exposes an activity LED and a Power button, which shuts down the device if held for at least three seconds.

![Side view](http://g4f.altervista.org/wp-content/uploads/2018/04/photo_2018-04-13_21-23-56-e1523655002423.jpg)

Finally, the top layer hosts the battery, which is probably the bulkiest piece. Two side holes provide space for a charge connector and the battery power slider. Some adjustment had to be done manually to properly fit the battery and all the connectors.

![Top view](http://g4f.altervista.org/wp-content/uploads/2018/04/photo_2018-04-13_21-23-58-e1523655013540.jpg)

The reader hosts a small server which can be polled for the current readings, just connect on TCP port 7777. As a proof-of-concept, a small app has also been designed to be run on an Android device. The app expects the scanner to be moved on a sample European map, showing which point is currently reading, and provided one of the predefined states is highlighted it will load its Wikipedia page.

![App view](http://g4f.altervista.org/wp-content/uploads/2018/04/photo_2018-04-13_21-23-09.jpg)
![Scanner sample](http://g4f.altervista.org/wp-content/uploads/2018/04/photo_2018-04-13_21-23-50.jpg)

## Building and running

### Premise
First off if you want to try this yourself I cannot provide you with a dotted version of the Europe map, you'll have to overlay it yourself using Libdots' tools. This is because the pattern is currently patented and I cannot redistribute it.

Secondly libdots was not tested on ARM architectures and has some low-level issues with data sizes that I had to scavenge myself. You can find a list of these issues and how to fix them in the NOTES.md file.

### The shopping cart

These are the pieces of hardware for which the casing has been designed. You can make your own choice but you will have to rearrange the holes for the mounting yourself.

* [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/)
* [ELP Fisheye Camera](http://www.elpcctv.com/170-degree-fisheye-lens-otg-vga-usb-camera-module-support-yuy-and-mjeg-p-191.html)
* [Quimat Battery Pack](https://www.amazon.it/gp/product/B06VVMHPFR)

Other items (resistors, LEDs, buttons and relay) are just consumer electronics that can be found in any specialized shop.

### Printing the casing

You will find the casing files inside the model subdirectory. There's the SketchUp project file if you need to do any modification or the already cleaned up .stl files for 3D printing which you'll have to convert and feed to your 3D printer.

### Programming the Pi

Testing has been done on Raspbian Stretch 2017-11-29. All source files are within the raspberry subdirectory. You will need OpenCV installed (just compile and install the latest version, beware it might take a while) as well as libdots. To compile the reader you need to provide the location of libdots headers to include, just run with:

```bash
./compile.sh <your-libdots-header-directory>
```

The compilation output will be in build/LibdotsReader. You can test whether the software is working with the provided test picture, just run like ./build/LibdotsReader -test testScreen.png. It should output a successfull decoding of the testScreen.png picture.

You will need to place the cameraSwitch.py script AND your LibdotsReader executable within /usr/local/bin. Remember to add execute permissions with chmod +x. You also need to place an init.d script to run cameraSwitch.py on startup so that the reader service is automatically started. You can read more here:

[Sample service script for debianoids](https://gist.github.com/naholyr/4275302)

You will also need to do the same for shutdownSwitch.py. This will enable the shutdown switch which we'll later wire into the Pi. By default it powers off the system after being held for 3 seconds, but you can change the value yourself.

### Wiring up the Pi

You can find a Fritzing scheme of all connections inside the raspberry subdirectory. You will need a normally open push-button for the poweroff (labelled as S1) as well as a pressure switch (labelled S2) to control the lighting system. To avoid overloading the Pi when powering the 4 white LEDs, I placed a relay with two slave outputs. The relay is controlled by a 5V USB source which is interrupted by the pressure switch. When the switch closes, the relay both closes the circuit powering up the LEDs and shorts GPIO 16 to ground, which can be used as an input to start reading the camera.

Please note that putting all together required a lot of manual tinkering. First off, I pulled off the LED power from the secondary USB output of the battery pack. The LEDs had (of course) to be soldered together as the space is extremely tight. The camera cable had to be handy-crafted as the Pi Zero expects a USB micro while the provided one is a regular, bulky USB (again, I soldered the connector to a USB micro cable). Finally, I had to remove the battery's original power slider and cable an external one that would be easier to reach.

When everything is assembled probing the camera's output should provide something like this:

![Sample picture](http://g4f.altervista.org/wp-content/uploads/2018/04/libdots.png)

Note that only the small portion in the middle is our ROI. Also you'll have to tinker yourself if you want to test the camera manually since I didn't provide support to stream the image (this picture here was extracted by some other tool I had built for a similar project).


### Printing the europe map

You will need libdots' overlayer tool for this. Just run it with regular settings on the map's PDF and print it on a high-resolution printer (600 dpi or more). Some extra libraries are required to compile it which are not-so-easy to configure on Raspbian, I would advice doing so separately on Ubuntu.

### Connecting with your phone

If you pulled through all up to now, this will be a breeze. Just open the LibdotsEuropeApp folder with Android Studio, and compile for your phone. You will need to be connected to the same wireless network as your Pi (using the phone's hotspot is fine) which will have to be configured beforehand. Type the Pi's IP into the text bar and tap 'Connect'. The phone should hook up on the Pi's camera reader and start sending data. If you move the scanner on top of the chart, you should see the crosshair following your movements. Just make sure the pressure switch is closed.

If it works, congratulations! You just made through all my hassles to build an absolutely inefficient and impractical device for scanning through a piece of paper :D Jokes apart, guys at Chili's have made an amazing job with their Cellulo which is way more interesting than this monstrosity. But I think my project is still somewhat fun to assemble and start getting to know Raspberry and 3D printing.