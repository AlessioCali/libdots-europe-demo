#include "LibdotsReader.h"

void printHelp();
char* programName;

enum class Commands : char {
	OK = 0,
	KO = 1
};

int main (int argc, char** argv) {
    int camNo = 0, thresh = 0;
    bool test = false, gui = false;
    std::string testName;
    programName = argv[0];

    if (argc < 2) {
        printHelp();
        return 0;
    }
    
    camNo = atoi(argv[1]);
    if (camNo < 0) {
        printHelp();
        return 0;
    }

    int i = 1;
    while (++i < argc) {
        if (strcmp(argv[i], "-t") == 0) {
            if (i >= argc) {
                printHelp();
                return 0;
            }
            else {
                thresh = atoi(argv[++i]);
                if (thresh <= 0) {
                    printHelp();
                    return 0;
                }
            }
        }
        else if (strcmp(argv[i], "-test") == 0) {
            if (i >= argc) {
                printHelp();
                return 0;
            }
            else {
                testName = std::string(argv[++i]);
                test = true;
            }
        }
        else if (strcmp(argv[i], "-gui") == 0) {
            gui = true;
        }
    }

    LibdotsReader reader(camNo, gui, thresh);
    if (test) { reader.test(testName); }
    else { while ( reader.run()  && (std::cin.get() == static_cast<char>(Commands::OK)) ); }
    return 0;
}

void printHelp() {
    std::cout << "Usage: " << programName << " <cameraIdx> [-t <grayThreshold> | -gui | -test <testImage>]" << std::endl;
    return;
}
