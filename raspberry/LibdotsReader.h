#ifndef LIBDOTS_READER_H
#define LIBDOTS_READER_H

#include <opencv2/opencv.hpp>

#include <dots_localization.h>
#include <config.h>

class LibdotsReader {

public: 

LibdotsReader(int camNo = 0, bool gui = false, int grayThresh = 0);

~LibdotsReader();

bool run();
bool test(std::string testFile);

private:

cv::VideoCapture capture;
DotsPositionInfo dotsPosInfo;
double dWidth, dHeight;
bool gui;
int grayThresh;

bool getRawFrame(cv::Mat& frame, cv::Rect roi);
void decode(cv::Mat rawFrame);
void process(cv::Mat rawFrame, cv::Mat& dstFrame);
void drawFrame(cv::Mat& frame, std::string title);
void printInfo(DotsPositionInfo *info);

};

#endif
