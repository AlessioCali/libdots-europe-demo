#!/usr/bin/env python

from enum import Enum
from gpiozero import Button
import subprocess
import socket
import time

INPUT_PIN = 23

class Commands(Enum):
    OK = 0
    KO = 1

def xml_process(input_data):
    # TODO: Process input data
    return input_data

def activate():
    global active
    active = True

def deactivate():
    global active
    active = False

reader_process = subprocess.Popen(["LibdotsReader", "0"], stdout=subprocess.PIPE, stdin=subprocess.PIPE)
active = False

laydown_button = Button(INPUT_PIN)

# When ground button is open, input pin is open
laydown_button.when_released = deactivate
# When ground button is closed, input pin goes to ground, so it's closed
laydown_button.when_pressed = activate

server = socket.socket()
server.bind(('', 7777))
server.listen(5)

while True:
    client, _ = server.accept()
    try:
        while True:
            if active:
                reader_process.stdin.write(buffer(bytearray([Commands.OK.value])))
                reader_process.stdin.flush()
                reader_input = reader_process.stdout.readline()
                processed_data = xml_process(reader_input)
                client.sendall(processed_data)
            else:
                client.sendall("DEVICE_OFF\n".encode())
                time.sleep(1)
    except:
        pass
    finally:
        client.close()
        
reader_process.terminate()
server.close()
