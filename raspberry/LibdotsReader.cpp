#include "LibdotsReader.h"

#include <stdio.h>
#include <iostream>

#define GRID_SIZE 0.508

#define ROI_WIDTH 160
#define ROI_HEIGHT 120

#define SCALE 2

std::map<int, std::string> errorCodes = {
	{ DECODING_NO_ERROR, "DECODING_NO_ERROR" },
	{ DECODING_TOO_MANY_POINTS, "DECODING_TOO_MANY_POINTS" },
	{ DECODING_NOT_ENOUGH_POINTS, "DECODING_NOT_ENOUGH_POINTS" },
	{ DECODING_COLLINEAR_GRID, "DECODING_COLLINEAR_GRID" },
	{ DECODING_WRONG_GRID_SIZE, "DECODING_WRONG_GRID_SIZE" },
	{ DECODING_X_FAILED, "DECODING_X_FAILED" },
	{ DECODING_Y_FAILED, "DECODING_Y_FAILED" },
	{ DECODING_BOTH_FAILED, "DECODING_BOTH_FAILED" },
	{ NO_INFO, "NO_INFO"},
};

LibdotsReader::LibdotsReader(int camNo, bool gui, int grayThresh) {
    capture = cv::VideoCapture(camNo);
    if (!capture.isOpened()) {
        //std::cout << "CAMERA_ERROR" << std::endl;
    }
    dWidth = capture.get(CV_CAP_PROP_FRAME_WIDTH);
    dHeight = capture.get(CV_CAP_PROP_FRAME_HEIGHT);

    this->gui = gui;
    this->grayThresh = grayThresh;

    //std::cout << "CAMERA_OK" << " " << dWidth << " " << dHeight << std::endl;

    dotsPosInfo = dotsPosInfoInit();    
}

LibdotsReader::~LibdotsReader() {
    capture.release();
}

bool LibdotsReader::run() {
    if (cv::waitKey(10) != -1) { return false; }
    cv::Rect roi = cv::Rect(dWidth / 2 - ROI_WIDTH / 2, dHeight / 2 - ROI_HEIGHT / 2, ROI_WIDTH, ROI_HEIGHT);
    cv::Mat rawFrame;

    if (!getRawFrame(rawFrame, roi)) {
        std::cout << "-1 -1 -1 " << "CAMERA_ERROR" << std::endl;
        return true;
    }

    decode(rawFrame);
    return true;
}

bool LibdotsReader::test(std::string testFile) {
    cv::Mat rawFrame = cv::imread(testFile);
    if (rawFrame.data == NULL) {
        std::cout << "Could not read source image " << testFile << "!" << std::endl;
        return false;
    }

    decode(rawFrame);
    return true;
}

void LibdotsReader::decode(cv::Mat rawFrame) {
    cv::Mat frame;

    drawFrame(rawFrame, "Original image");
    process(rawFrame, frame);
    drawFrame(frame, "Processed image");

    std::vector<cv::Mat> channels;
    cv::split(frame, channels);
    unsigned char* image = channels[0].ptr();
    int rows = channels[0].rows;
    int cols = channels[0].cols;

    DotsPositionInfo* ptPosInfo = &dotsPosInfo;
    ptPosInfo = dotsLocalize(ptPosInfo, image, rows, cols);

    printInfo(ptPosInfo);

    return;
}

bool LibdotsReader::getRawFrame(cv::Mat& frame, cv::Rect roi) {
    bool ans = false;
    cv::Mat tmp;
    if (capture.isOpened()) {
        ans = capture.read(tmp);
        frame = tmp(roi).clone();
    }
    return ans;
}

void LibdotsReader::drawFrame(cv::Mat& frame, std::string title) {
    if (gui) { cv::imshow(title, frame); }
}

void LibdotsReader::process(cv::Mat rawFrame, cv::Mat& dstFrame) {
    dstFrame = rawFrame.clone();
    
    //cv::cvtColor(dstFrame, dstFrame, cv::COLOR_BGR2GRAY);
    //cv::blur(dstFrame, dstFrame, cv::Size(3, 3));
    //cv::equalizeHist(dstFrame, dstFrame);
    
    if (grayThresh != 0) {
        cv::cvtColor(dstFrame, dstFrame, cv::COLOR_BGR2GRAY);
        cv::threshold(dstFrame, dstFrame, grayThresh, 255, cv::THRESH_BINARY);
        cv::cvtColor(dstFrame, dstFrame, cv::COLOR_GRAY2BGR);
    }

    cv::resize(dstFrame, dstFrame, cv::Size(SCALE * rawFrame.cols, SCALE * rawFrame.rows), cv::INTER_LINEAR);
    //cv::cvtColor(dstFrame, dstFrame, cv::COLOR_GRAY2BGR);
}

void LibdotsReader::printInfo(DotsPositionInfo *info) {
    float robotX = ((float)dotsGetIntX(info))/FIXED_RESULT_DIVISOR * GRID_SIZE;
    float robotY = ((float)dotsGetIntY(info))/FIXED_RESULT_DIVISOR * GRID_SIZE;
    float robotTheta = dotsGetAngle(info)/3.14159*180;

    std::cout << robotX << " " << robotY << " " << robotTheta << " ";

    std::cout << errorCodes[info->decodingError] << std::endl;
/*
    switch(info->decodingError){
        case DECODING_NO_ERROR:
            std::cout << static_cast<int>(ErrorCodes::DECODING_NO_ERROR) << std::endl;
            break;
        case DECODING_TOO_MANY_POINTS:
            std::cout << static_cast<int>(ErrorCodes::DECODING_TOO_MANY_POINT) << std::endl;
            break;
        case DECODING_NOT_ENOUGH_POINTS:
            std::cout << static_cast<int>(ErrorCodes::DECODING_NOT_ENOUGH_POINTS) << std::endl;
            break;
        case DECODING_COLLINEAR_GRID:
            std::cout << static_cast<int>(ErrorCodes::DECODING_COLLINEAR_GRID) << std::endl;
            break;
        case DECODING_WRONG_GRID_SIZE:
            std::cout << static_cast<int>(ErrorCodes::DECODING_WRONG_GRID_SIZE) << std::endl;
            break;
        case DECODING_X_FAILED:
            std::cout << static_cast<int>(ErrorCodes::DECODING_X_FAILED) << std::endl;
            break;
        case DECODING_Y_FAILED:
            std::cout << static_cast<int>(ErrorCodes::DECODING_Y_FAILED) << std::endl;
            break;
        case DECODING_BOTH_FAILED:
            std::cout << static_cast<int>(ErrorCodes::DECODING_BOTH_FAILED) << std::endl;
            break;
        case NO_INFO:
            std::cout << static_cast<int>(ErrorCodes::NO_INFO) << std::endl;
            break;
    }*/
}
