#!/usr/bin/env python

from gpiozero import Button
from subprocess import check_call
from signal import pause

SHUTDOWN_PIN = 3

def call_shutdown():
    check_call(["sudo", "poweroff"])

shutdown_button = Button(SHUTDOWN_PIN, hold_time=3)
shutdown_button.when_held = call_shutdown

pause()
