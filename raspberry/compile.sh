# Run this script and provide libdot's headers location as first argument. Any other argument can be passed as second.

g++ main.cpp LibdotsReader.cpp -o build/LibdotsReader `pkg-config --libs opencv` -ldots -I$1 $2
