package agc.libdots_europe_app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ImageView;

import org.jetbrains.annotations.Nullable;


public class EuropeView extends ImageView {

    public enum States {
        ITALY(64, 162, 203, 247, "Italy"),
        SPAIN(95, 148, 83, 149, "Spain"),
        FRANCE(140, 195, 155, 209, "France"),
        GERMANY(176, 225, 215, 253, "Germany"),
        UK(214, 285, 150, 186, "United_Kingdom"),
        IRELAND(237, 263, 121, 150, "Ireland"),
        ICELAND(358, 387, 132, 166, "Iceland"),
        TURKEY(80, 123, 357, 474, "Turkey"),
        RUSSIA(219, 394, 359, 517, "Russia");

        // All of these are in  print space
        // Conversion from (x, y) in print space to (x', y') in view space is:
        // (x', y') = (y, MAX_X - x) rescaled by view size
        //
        // x /\                     |-------------> x'
        //   |  PRINT         ==>   |  VIEW
        //   |                      |
        //   ------------> y        \/ y'

        public final float  x1, // Left
                            x2, // Right
                            y1, // Bottom
                            y2; // Top
        public final String countryName;
        private static final String WIKI_PREFIX = "https://en.wikipedia.org/wiki/";

        private States(float x1, float x2, float y1, float y2, String countryName) {
            this.x1 = x1;
            this.x2 = x2;
            this.y1 = y1;
            this.y2 = y2;
            this.countryName = countryName;
        }

        // Return complete wikipedia URL for this state
        public String getCountryUri() { return WIKI_PREFIX + countryName; }

        // Check if a given point in raw print space is inside this State
        public boolean isInside(float x, float y) {
            return x1 <= x && x <= x2 && y1 <= y && y <= y2;
        }

        // Return the rectangle to paint for this state in view space
        public Rect getRect(float containerWidth, float containterHeight) {
            return new Rect((int)(y1 / MAX_Y * containerWidth),
                            (int)((1 - x2 / MAX_X) * containterHeight),
                            (int)(y2 / MAX_Y * containerWidth),
                            (int)((1 - x1 / MAX_X) * containterHeight));
        }
    }

    private float x = 0, y = 0;
    private float mapSpaceX = 0, mapSpaceY = 0;
    private Paint crosshairPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint countryPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint selectedCountryPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private States selectedState = null;

    private final static float MAX_Y = 584, MAX_X = 413;

    public EuropeView(Context context) {
        this(context, null, 0, 0);
    }

    public EuropeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0, 0);
    }

    public EuropeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public EuropeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        crosshairPaint.setColor(Color.BLUE);
        countryPaint.setColor(Color.RED);
        countryPaint.setStyle(Paint.Style.STROKE);
        selectedCountryPaint.setColor(Color.GREEN);
        selectedCountryPaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(x, 0, x, getHeight(), crosshairPaint);
        canvas.drawLine(0,  y, getWidth(), y, crosshairPaint);

        // Draw all states on the map, red unselected green selected
        selectedState = null;
        for (States state : States.values()) {
            if (state.isInside(mapSpaceX, mapSpaceY)) {
                canvas.drawRect(state.getRect(getWidth(), getHeight()), selectedCountryPaint);
                selectedState = state;
            }
            else {
                canvas.drawRect(state.getRect(getWidth(), getHeight()), countryPaint);
            }
        }
    }

    public void setData(float x, float y) {
        this.x = y / MAX_Y * getWidth();
        this.y = (1 - x / MAX_X) * getHeight();
        this.mapSpaceX = x;
        this.mapSpaceY = y;
        postInvalidate();
    }

    public States getSelectedState() {
        return selectedState;
    }

}
