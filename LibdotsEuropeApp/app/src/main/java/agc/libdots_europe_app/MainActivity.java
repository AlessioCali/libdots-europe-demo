package agc.libdots_europe_app;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.logging.Logger;

public class MainActivity extends Activity {

    private float x, y;
    private ConnectThread updater = null;

    // This thread is used to connect to the libdot reader. Default port is set to 7777
    private class ConnectThread extends Thread {
        private boolean running = true;
        private EuropeView.States oldState = null;

        public void halt() { running = false; }

        @Override
        public void run() {
            Socket s = null;
            TextView ipView = findViewById(R.id.ipView);
            Button connectButton = findViewById(R.id.connectButton);
            try {
                // Number of all readings / good readings for error percentages.
                long nx = 0, ny = 0, gx = 0, gy = 0;
                double px, py;
                EuropeView eu = findViewById(R.id.europeView);
                TextView outputView = findViewById(R.id.outputView);
                WebView countryWikiView = findViewById(R.id.wiki_web_view);
                s = new Socket();
                s.connect(new InetSocketAddress(ipView.getText().toString(), 7777), 1000);
                connectButton.post(() -> connectButton.setText("Connected"));
                BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
                while(running) {
                    String[] input = in.readLine().split(" ");
                    if (input[0].equals("DEVICE_OFF")) {
                        continue;
                    }
                    Float x = Float.parseFloat(input[0]);
                    Float y = Float.parseFloat(input[1]);
                    Float theta = Float.parseFloat(input[2]);
                    eu.setData(x, y);
                    nx++;
                    ny++;
                    if (input[3].equals("DECODING_X_FAILED")) { gy++; }
                    else if (input[3].equals("DECODING_Y_FAILED")) { gx++; }
                    else if (input[3].equals("DECODING_NO_ERROR")) { gx++; gy++; }

                    // Current success rate
                    px = (100.0 * gx) / nx;
                    py = (100.0 * gy) / ny;

                    final double fpx = px;
                    final double fpy = py;
                    String countryName = eu.getSelectedState() != null ? eu.getSelectedState().countryName : "NO_STATE";

                    // Show data on output
                    outputView.post(() -> outputView.setText(
                            String.format(
                                    "X: %.2f | Y : %.2f\n" +
                                    "EX: %.2f %% | EY : %.2f %%\n" +
                                    "%s\n" +
                                    "%s",
                                    x, y, fpx, fpy, input[3], countryName))
                    );

                    // Update web view if a new state was selected
                    if (eu.getSelectedState() != null && oldState != eu.getSelectedState()) {
                        oldState = eu.getSelectedState();
                        countryWikiView.post(() -> {
                            countryWikiView.loadUrl(eu.getSelectedState().getCountryUri());
                        });
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                try { if (s != null) { s.close(); } } catch(Exception se) { se.printStackTrace(); }
                MainActivity.this.reset();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView wb = findViewById(R.id.wiki_web_view);
        wb.setWebViewClient(new WebViewClient());
    }

    synchronized void setData(float x, float y) {
        this.x = x;
        this.y = y;
    }

    synchronized Pair<Float, Float> getData() {
        return new Pair<>(x, y);
    }

    public void connect(View v) {
        TextView ipView = findViewById(R.id.ipView);
        Button connectButton = findViewById(R.id.connectButton);
        connectButton.setText("Connecting...");
        connectButton.setEnabled(false);
        updater = new ConnectThread();
        updater.start();
    }

    public void reset() {
        if (updater != null && !updater.isInterrupted()) {
            try { updater.interrupt(); }
            catch (Exception e) { e.printStackTrace(); }
        }
        Button connectButton = findViewById(R.id.connectButton);
        connectButton.post(() -> {
            connectButton.setText("Connect");
            connectButton.setEnabled(true);
        });
    }

    public void haltBackgroundThread(View v) {
        if (updater != null) {
            updater.halt();
            try { updater.join(1000); }
            catch (InterruptedException ie) { ie.printStackTrace(); }
            finally { updater.interrupt(); }
            updater = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        haltBackgroundThread(null);
    }
}
